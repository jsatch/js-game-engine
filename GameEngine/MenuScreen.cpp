//
//  MenuScreen.cpp
//  GameEngine
//
//  Created by Hernan Quintana on 10/22/18.
//  Copyright © 2018 Hernan Quintana. All rights reserved.
//

#include "MenuScreen.hpp"

MenuScreen::MenuScreen(SDL_Renderer* renderer) : JSScreen(renderer)
{
    const std::string ruta ="assets/ArtTileset.png";
    fondo = new JSSimpleGameObject(ruta, renderer);
    
    add_game_object(fondo);
    fondo->get_rect()->x = 0;
    fondo->get_rect()->y = 0;
    fondo->get_rect()->w = 256;
    fondo->get_rect()->h = 256;
}
void MenuScreen::update(uint32_t ticks)
{
    //fondo->get_rect()->x++;
    std::cout << fondo->get_rect()->x << std::endl;
    JSScreen::update(ticks);
}
void MenuScreen::render()
{
    JSScreen::render();
}
MenuScreen::~MenuScreen()
{}
