//
//  MenuScreen.hpp
//  GameEngine
//
//  Created by Hernan Quintana on 10/22/18.
//  Copyright © 2018 Hernan Quintana. All rights reserved.
//

#ifndef MenuScreen_hpp
#define MenuScreen_hpp

#include <SDL2/SDL.h>
#include "Framework/JSScreen.hpp"
#include "Framework/JSSimpleGameObject.hpp"
#include <iostream>
#include <string>

class MenuScreen : public JSScreen
{
private:
    JSSimpleGameObject* fondo;
public:
    MenuScreen(SDL_Renderer* renderer);
    virtual void update(uint32_t ticks);
    virtual void render();
    ~MenuScreen();
    
};

#endif /* MenuScreen_hpp */
