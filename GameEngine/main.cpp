//
//  main.cpp
//  GameEngine
//
//  Created by Hernan Quintana on 10/14/18.
//  Copyright © 2018 Hernan Quintana. All rights reserved.
//

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include "Framework/JSGame.hpp"
#include "MenuScreen.hpp"




int main(int argc, const char * argv[]) {
    JSGame* game = new JSGame();
    game->set_start_controller(new MenuScreen(game->get_renderer()));
    game->start();
   
    return 0;
}
